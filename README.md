# 2018/11/03 - Experiment about dependency between Bolivian autonomous systems

To launch the measurements, just type:

```
make
```

To delete the data:

```
make clean
```

Some examples of the data produced:

- 04 Nov 2018, from Entel network (AS6568): [D008 - dependency between AS in Bolivia as seen from Entel](https://data.rednegra.net/2018/11/04/ASDependencyInBoliviaFromEntel.html).
- 12 Nov 2018, from Tigo / Telecel network (AS27882): [D012 - dependency between AS in Bolivia as seen from Tigo](https://data.rednegra.net/2018/11/12/ASDependencyInBoliviaFromTigo.html).
