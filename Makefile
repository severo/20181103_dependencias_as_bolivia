prefixesDir := prefixes
tracerouteDir := traceroute
asinfoDir := asinfo
dataDir := data
prefixesListFilename := prefixesList.txt
prefixesTracerouteFilename := prefixesTraceroute.json
asInTracerouteFilename := asInTraceroute.txt
asInfoFilename := asInfo.json

export prefixesList := $(addsuffix /$(prefixesListFilename), $(dataDir))
export prefixesTraceroute := $(addsuffix /$(prefixesTracerouteFilename), $(dataDir))
export asInTraceroute := $(addsuffix /$(asInTracerouteFilename), $(dataDir))
export asInfo := $(addsuffix /$(asInfoFilename), $(dataDir))

.PHONY: all
all: getASInfo

$(dataDir):
	mkdir -p $(dataDir)

$(prefixesList): | $(dataDir)
	$(MAKE) -C $(prefixesDir)

$(prefixesTraceroute): $(prefixesList)
	$(MAKE) -C $(tracerouteDir) -j 20

getASInfo: $(prefixesTraceroute)
	$(MAKE) -C $(asinfoDir) -j 20

.PHONY: clean cleanPrefixes cleanTraceoute cleanASInfo
clean: cleanASInfo cleanTraceroute cleanPrefixes
	rm -rf data

cleanPrefixes:
	$(MAKE) -C $(prefixesDir) clean

cleanTraceroute:
	$(MAKE) -C $(tracerouteDir) clean

cleanASInfo:
	$(MAKE) -C $(asinfoDir) clean
